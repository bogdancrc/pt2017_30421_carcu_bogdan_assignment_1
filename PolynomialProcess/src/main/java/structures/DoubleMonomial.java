package structures;

/*
 * Subclass of Monomial class.
 * Treats the case of real coefficients.
 * 
 */

public class DoubleMonomial extends Monomial{
	
	public DoubleMonomial(Double coefficient, int degree){
	
			setCoefficient(coefficient);
			setDegree(degree);
		
	}
	
	public String toString(){
		
		String result = "";
		
		if(getCoefficient().doubleValue() == 0)
			return "";
		
		if(getCoefficient().doubleValue() == 1)
			result += "X";
		else if(getCoefficient().doubleValue() == -1)
			result += "-X";
		else
			result += String.format("%.2f", getCoefficient().doubleValue()) + "X";
		
		if(getDegree() == 0)
			return "" + String.format("%.2f", getCoefficient().doubleValue());
		else if(getDegree() == 1)
			return result;
		else
			result += "^" + getDegree();
	
		return result;
	}

}
