package structures;

/*
 * Monomial class.
 * Abstract class whose structure is used to store
 * the coefficient and degree of a single monomial.
 * Provides setters and getters.
 * 
 */

public abstract class Monomial {

	private int degree;
	private Number coefficient;

	public void setCoefficient(Number coefficient){
		
		this.coefficient = coefficient;
		
	}
	
	public void setDegree(int degree){
		
		this.degree = degree;
		
	}
	
	public Number getCoefficient(){
		
		return coefficient;
		
	}
	
	public int getDegree(){
		
		return degree;
		
	}
	
	public abstract String toString();
	
}