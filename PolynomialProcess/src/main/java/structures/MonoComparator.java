package structures;

import java.util.Comparator;

/*
 * Class used to compare two Monomial objects in order to 
 * sort them decreasingly at the construction time of
 * a polynomial
 * 
 */

public class MonoComparator implements Comparator<Monomial> {

	public int compare(Monomial m1, Monomial m2) {
		
		return m1.getDegree() < m2.getDegree() ? 1 : -1;
		
	}

}
