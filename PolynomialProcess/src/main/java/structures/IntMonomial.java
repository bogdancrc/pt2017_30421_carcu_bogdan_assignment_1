package structures;

/*
 * Subclass of Monomial class.
 * Treats the case of integer coefficients 
 * 
 */

public class IntMonomial extends Monomial{

	public IntMonomial(Integer coefficient, int degree){
		
			setCoefficient(coefficient);
			setDegree(degree);
	
	}

	public String toString(){
		
		String result = "";
		
		if(getCoefficient().intValue() == 0)
			return "";
		
		if(getCoefficient().intValue() == 1)
			result += "X";
		else if(getCoefficient().intValue() == -1)
			result += "-X";
		else
			result += getCoefficient().intValue() + "X";
		
		if(getDegree() == 0)
			return "" + getCoefficient().intValue();
		else if(getDegree() == 1)
			return result;
		else
			result += "^" + getDegree();
		
		return result;
		
	}
}
