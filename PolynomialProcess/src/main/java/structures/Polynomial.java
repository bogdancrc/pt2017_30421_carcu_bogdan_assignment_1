package structures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * Polynomial class.
 * Manipulates lists of Monomial objects.
 * Uses polymorphic properties of Monomial class.
 * Implements basic polynomial operations.
 * 
 * */

public class Polynomial {

	private List<Monomial> monomials;		
	
	/*
	 * Constructors
	 * */
	public Polynomial(){				
		
		monomials = new ArrayList<Monomial>();
		
	}
	
	public Polynomial(Monomial m){
		
		monomials = new ArrayList<Monomial>();
		monomials.add(m);
		
	}
	
	public Polynomial(List<Monomial> mList){  		
		
		monomials = new ArrayList<Monomial>(sortMonomials(mList));
		
	}
	
	/*
	 * Method to refresh 'monomials' by eliminating 0 coefficient elements
	 */
	private void refreshList(){						
		
		List<Monomial> newList = new ArrayList<Monomial>();
		
		for(Monomial m: monomials)
			if(m.getCoefficient().doubleValue() != 0)
				newList.add(m);
		
		monomials = newList;
		
	}
	
	/*
	 * Sorts monomials in descending order
	 * by their degree
	 */
	private List<Monomial> sortMonomials(List<Monomial> mList){		
		
		List<Monomial> sortedList = new ArrayList<Monomial>();
		sortedList.addAll(mList);
		
		Collections.sort(sortedList, new MonoComparator());
		
		return sortedList;
		
	}
	
	/*
	 * Changes every coefficient to its opposite ('+' -> '-' and vice versa)
	 */
	private Polynomial invert(){
		
		List<Monomial> result = new ArrayList<Monomial>();
		
		for(Monomial m: monomials)
			result.add(new IntMonomial(0 - m.getCoefficient().intValue(), m.getDegree()));
			
		Polynomial pol = new Polynomial(result);
		return pol;
		
	}
	
	/*
	 * Performs addition of two polynomials
	 */
	public Polynomial add(Polynomial p){
		
		List<Monomial> result = new ArrayList<Monomial>();			
		List<Monomial> toBeRemoved = new ArrayList<Monomial>();		
		
		for(Monomial x: monomials)
			for(Monomial y: p.monomials)
				if(x.getDegree() == y.getDegree()){
					
					result.add(new IntMonomial(x.getCoefficient().intValue() + y.getCoefficient().intValue(), x.getDegree()));
					toBeRemoved.add(x);
					toBeRemoved.add(y);
					
				}
		
		result.addAll(monomials);
		result.addAll(p.monomials);
		result.removeAll(toBeRemoved);	
		
		Polynomial r = new Polynomial(result);	
			
		return r;
	}
		
	/*
	 * Performs subtraction of two polynomials
	 */
	public Polynomial subtract(Polynomial p){
		
		Polynomial pSub = p.invert();		
		return this.add(pSub);				
		
	}
	
	/*
	 * Performs polynomial multiplication
	 */
	public Polynomial multiply(Polynomial p){
		
		List<Monomial> result = new ArrayList<Monomial>();
		Polynomial old = new Polynomial();		
		Polynomial recent = new Polynomial();
		
		for(Monomial x: p.monomials){
			
			for(Monomial y: monomials){
				
				result.add(new IntMonomial(x.getCoefficient().intValue() * y.getCoefficient().intValue(), x.getDegree() + y.getDegree()));
			
			}
			
			recent = new Polynomial(result);
			result.removeAll(result);			
			old = old.add(recent);				
			
		}
		
		return old;
	}
	
	/*
	 * Performs polynomial division
	 */
	public Polynomial divide(Polynomial p){
			
		Polynomial q = new Polynomial();	//quotient, polynomial to be returned
		Polynomial demp = this;				//polynomial to be divided
		Monomial temp;
		Polynomial tempPol;					//polynomial with a single monomial (temp)
		double coeff; int exp;
		Monomial firstMonomial = new IntMonomial(p.monomials.get(0).getCoefficient().intValue(), p.monomials.get(0).getDegree());
		
		for(Monomial m: demp.monomials){
			
			if(m.getDegree() < firstMonomial.getDegree())	
					break;
			
			coeff = (double) demp.monomials.get(0).getCoefficient().doubleValue() / firstMonomial.getCoefficient().doubleValue();
			exp = m.getDegree() - firstMonomial.getDegree();	
			
			if(coeff == Math.floor(coeff))					
				temp = new IntMonomial((int)coeff, exp);	
			else
				temp = new DoubleMonomial(coeff, exp);		
			
			tempPol = new Polynomial(temp);					//create one-monomial polynomial
			q = q.add(tempPol);								//add it to the quotient
			tempPol = tempPol.multiply(p);					
			demp = demp.subtract(tempPol);					//compute and store the remainder in demp
			demp.refreshList();								//get rid of monomials with 0 coefficients
		}
		
		return q;
	}
	
	/*
	 * Performs the differentiation of a single polynomial
	 */
	public Polynomial differentiate(){
		
		List<Monomial> result = new ArrayList<Monomial>();
		
		for(Monomial m: monomials)
			if(m.getDegree() != 0)
				result.add(new IntMonomial(m.getCoefficient().intValue() * m.getDegree(), m.getDegree() - 1));

		return new Polynomial(result);
		
	}
	
	/*
	 * Performs the integration of a single polynomial
	 */
	public Polynomial integrate(){
		
		List<Monomial> result = new ArrayList<Monomial>();
		
		for(Monomial m: monomials){
			
			double coeff = m.getCoefficient().doubleValue() * (1 / (double)(m.getDegree() + 1));
			int exponent = m.getDegree() + 1;
			
				if(coeff == Math.floor(coeff))
					result.add(new IntMonomial((int)coeff, exponent));
				else
					result.add(new DoubleMonomial(coeff, exponent));
				
		}
				
		return new Polynomial(result);
		
	}
	
	public String toString(){
	
		String result = "";
		boolean isFirstIteration = true;
		
		for(Monomial m: monomials){
			
			if(m.getCoefficient().doubleValue() == 0)
				continue;
			
			if(isFirstIteration){
				
				result += m;
				isFirstIteration = false;
				continue;
			}
			
			if(m.getCoefficient().doubleValue() > 0)
				result += " + " + m;
			else
				result += " " + m;
		}
		
		return result;
	}
	
}
		
		
