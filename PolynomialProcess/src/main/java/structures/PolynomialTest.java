package structures;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class PolynomialTest {

	//First polynomoal: 4X^2 -12X + 9
	//Second polynomial: 2X -3
	
	Monomial m1, m2, m3, m4, m5;
	Polynomial p1, p2;
	
	@Before
	public void prepareVariables(){
		
		List<Monomial> list = new ArrayList<Monomial>();
		
		m1 = new IntMonomial(4, 2);
		m2 = new IntMonomial(-12, 1);
		m3 = new IntMonomial(9, 0);
		m4 = new IntMonomial(2, 1);
		m5 = new IntMonomial(-3, 0);
		
		list.add(m1); list.add(m2); list.add(m3);
		p1 = new Polynomial(list);
		list.clear();
		list.add(m4); list.add(m5);
		p2 = new Polynomial(list);
	}
	
	@Test
	public void additionTest(){
		
		assertEquals("4X^2 -10X + 6", p1.add(p2) + "");
		
	}
	
	@Test
	public void subtractionTest(){
		
		assertEquals("4X^2 -14X + 12", p1.subtract(p2) + "");
		
	}
	
	@Test
	public void multiplicationTest(){
		
		assertEquals("8X^3 -36X^2 + 54X -27", p1.multiply(p2) + "");
		
	}
	
	@Test
	public void divisionTest(){
		
		assertEquals("2X -3", p1.divide(p2) + "");
		
	}
	
	@Test
	public void differentiationTest(){
		
		assertEquals("8X -12", p1.differentiate() + "");
		
	}
	
	@Test
	public void integrationTest(){
		
		assertEquals("X^2 -3X", p2.integrate() + "");
		
	}

}
