package gui;

import java.awt.event.*;
import structures.*;

/*
 * Class that links View and Model classes 
 * Offers functionality to the user interface.
 * 
 */
public class Controller {
	
	private View theView;
	
	public Controller(final View theView, final Model theModel){
		
		this.theView = theView;
		
		this.theView.setAddListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				Polynomial p1, p2;
				
				try{
					
					p1 = theView.getPolynomialOne();
					p2 = theView.getPolynomialTwo();
					
					theModel.addPolynomials(p1, p2);
					theView.setResult(theModel.getResult());
					
					
				} catch(Exception ex){
					
					theView.sendErrorMsg("Addition cannot be done due to incorrect input.");
					
				}
				
			}

		});
		
		this.theView.setSubListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				Polynomial p1, p2;
				
				try{
					
					p1 = theView.getPolynomialOne();
					p2 = theView.getPolynomialTwo();
					
					theModel.subtractPolynomials(p1, p2);
					theView.setResult(theModel.getResult());
					
					
				} catch(Exception ex){
					
					theView.sendErrorMsg("Subtraction cannot be done due to incorrect input.");
					
				}
				
			}

		});
		
		this.theView.setMulListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				Polynomial p1, p2;
				
				try{
					
					p1 = theView.getPolynomialOne();
					p2 = theView.getPolynomialTwo();
					
					theModel.multiplyPolynomials(p1, p2);
					theView.setResult(theModel.getResult());
					
					
				} catch(Exception ex){
					
					theView.sendErrorMsg("Multiplication cannot be done due to incorrect input.");
					
				}
				
			}

		});
		
		this.theView.setDivListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				Polynomial p1, p2;
				
				try{
					
					p1 = theView.getPolynomialOne();
					p2 = theView.getPolynomialTwo();
					
					theModel.dividePolynomials(p1, p2);
					theView.setResult(theModel.getResult());
					
					
				} catch(Exception ex){
					
					theView.sendErrorMsg("Division cannot be done due to incorrect input.");
					
				}
				
			}

		});
		
		this.theView.setDiffListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				Polynomial p1;
				
				try{
					
					p1 = theView.getPolynomialOne();
					
					theModel.differentiatePolynomial(p1);
					theView.setResult(theModel.getResult());
					
					
				} catch(Exception ex){
					
					theView.sendErrorMsg("Differentiation cannot be done due to incorrect input.");
					
				}
				
			}

		});
		
		this.theView.setIntegListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e){
				
				Polynomial p1;
				
				try{
					
					p1 = theView.getPolynomialOne();
					
					theModel.integratePolynomial(p1);
					theView.setResult(theModel.getResult());
					
					
				} catch(Exception ex){
					
					theView.sendErrorMsg("Integration cannot be done due to incorrect input.");
					
				}
				
			}

		});
		
	}
	
}
