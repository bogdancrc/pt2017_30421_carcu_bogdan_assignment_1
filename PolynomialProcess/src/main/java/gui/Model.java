package gui;

import structures.*;

/*
 * Class that provides the computational structure
 * behind the user interface.
 * 
 */
public class Model {
	
	private Polynomial theResult;

	public Polynomial getResult(){
		
		return theResult;
		
	}
	
	public void addPolynomials(Polynomial p1, Polynomial p2){
		
		theResult = p1.add(p2);
		
	}
	
	public void subtractPolynomials(Polynomial p1, Polynomial p2){
		
		theResult = p1.subtract(p2);
		
	}
	
	public void multiplyPolynomials(Polynomial p1, Polynomial p2){
		
		theResult = p1.multiply(p2);
		
	}
	
	public void dividePolynomials(Polynomial p1, Polynomial p2){
		
		theResult = p1.divide(p2);
		
	}
	
	public void integratePolynomial(Polynomial p){
		
		theResult = p.integrate();
		
	}
	
	public void differentiatePolynomial(Polynomial p){
		
		theResult = p.differentiate();
		
	}
	
}
