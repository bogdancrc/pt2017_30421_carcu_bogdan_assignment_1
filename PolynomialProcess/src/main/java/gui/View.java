package gui;

import java.awt.event.*;
import javax.swing.*;
import structures.*;
import java.util.List;
import java.util.ArrayList;

/*
 * Class that builds the graphical user interface.
 * 
 */
@SuppressWarnings("serial")
public class View extends JFrame{

	private JPanel p1, p2, p3, p4, pFinal;
	private JLabel pol1, pol2, result;
	private JTextField polynomialOne, polynomialTwo, myResult;
	private JButton add, min, mul, div, integ, diff;
	
	public View(){
	
		super("Bogdan's Polymaniac");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(700, 110);
		setResizable(false);
		
		pol1 = new JLabel("Polynomial #1");
		pol2 = new JLabel("Polynomial #2");
		result = new JLabel("Result: ");
		
		polynomialOne = new JTextField(20);
		polynomialOne.setToolTipText("Insert every coefficient and power, even coefficient 1 and exponent 0");
		polynomialTwo = new JTextField(20);
		polynomialTwo.setToolTipText("Insert every coefficient and power, even coefficient 1 and exponent 0");
		myResult = new JTextField(20);
		myResult.setEditable(false);
		
		add = new JButton("+");
		add.setToolTipText("Add polynomials");
		min = new JButton("-");
		min.setToolTipText("Substract polynomials");
		mul = new JButton("x");
		mul.setToolTipText("Multiply polynomials");
		div = new JButton("÷");
		div.setToolTipText("Divide polynomials. The remainder will not be displayed!");
		integ = new JButton("∫");
		integ.setToolTipText("Integrate polynomial #1");
		diff = new JButton("d/dx");
		diff.setToolTipText("Differentiate polynomial #1");
	
		p1 = new JPanel(); p2 = new JPanel(); p3 = new JPanel(); p4 = new JPanel();
		pFinal = new JPanel();
		
		p1.add(pol1);
		p2.add(pol2);
		p3.add(add); p3.add(min); p3.add(mul);
		p3.add(div); p3.add(diff); p3.add(integ);
		p4.add(result);
		
		pFinal.add(p1); 
		pFinal.add(polynomialOne);
		pFinal.add(p2);
		pFinal.add(polynomialTwo);
		pFinal.add(p3);
		pFinal.add(p4);
		pFinal.add(myResult);
		
		add(pFinal);
		setVisible(true);
		
	}
	
	/*
	 * Method that helps converting the user input
	 * to a Polynomial object
	 */
	private Polynomial toPolynomial(String str){
		
		List<Monomial> result = new ArrayList<Monomial>();
		String s = str.replaceAll("X\\^",",");
    	s = s.replaceAll("\\+", ",");
    	s = s.replaceAll("\\-", ",-") + ',';	//change the input String to a series of numbers separated by commas
    	String holder = "";
    	int coeff = 0, exp, change = 0;
    	
    	for(int i=0; i<s.length(); ++i){
    		
    		if(s.charAt(i) == ' ')
    			continue;
    		
    		if(s.charAt(0) == ',' && i == 0)
    			continue;
    		
    		if(s.charAt(i) == ',')
    			if(change == 0){
    				coeff = Integer.parseInt(holder);
    				holder = "";
    				change = 1 - change;
    			}
    			else{
    				exp = Integer.parseInt(holder);
    				result.add(new IntMonomial(coeff, exp));
    				holder = "";
    				change = 1 - change;
    			}
    		else
    			holder += s.charAt(i);
    	}
    	return new Polynomial(result);
	}
	
	public Polynomial getPolynomialOne(){
		
		return toPolynomial(polynomialOne.getText());
		
	}
	
	public Polynomial getPolynomialTwo(){
		
		return toPolynomial(polynomialTwo.getText());
		
	}
	
	public void setResult(Polynomial result){
		
		if(!result.toString().equals(""))	
			myResult.setText(result + "");
		else
			myResult.setText("0");
		
	}
	
	public void sendErrorMsg(String msg){
		
		JOptionPane.showMessageDialog(null, msg, "Oops!", JOptionPane.ERROR_MESSAGE);
	
	}
	
	//series of methods to be further used by the Controller class
	public void setAddListener(ActionListener listenForButton){
		
		add.addActionListener(listenForButton);
	}
	
	public void setSubListener(ActionListener listenForButton){
		
		min.addActionListener(listenForButton);
	}
	
	public void setMulListener(ActionListener listenForButton){
	
		mul.addActionListener(listenForButton);
	}
	
	public void setDivListener(ActionListener listenForButton){
	
		div.addActionListener(listenForButton);
	}
	
	public void setDiffListener(ActionListener listenForButton){
		
		diff.addActionListener(listenForButton);
	}
	
	public void setIntegListener(ActionListener listenForButton){
		
		integ.addActionListener(listenForButton);
	}
	
}
